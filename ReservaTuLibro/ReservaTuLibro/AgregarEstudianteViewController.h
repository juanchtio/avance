//
//  AgregarEstudianteViewController.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/12/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "estudiante.h"
#import "ViewController.h"

@interface AgregarEstudianteViewController : UIViewController <UITextFieldDelegate>{
    estudiante * agregarEstudiante;
}

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtApellido;
@property (weak, nonatomic) IBOutlet UITextField *txtCarrera;
@property (weak, nonatomic) IBOutlet UITextField *txtCedula;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)btnRegistrar:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *mensaje;

@end
