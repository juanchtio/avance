//
//  AgregarEstudianteViewController.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/12/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "AgregarEstudianteViewController.h"
#import "estudiante.h"



@interface AgregarEstudianteViewController ()

@end

@implementation AgregarEstudianteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    agregarEstudiante=[[estudiante alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//button
- (IBAction)btnRegistrar:(id)sender {
    
    agregarEstudiante.studentsName=_txtName.text;
    agregarEstudiante.studentsLastName=_txtApellido.text;
    agregarEstudiante.studentsFaculty=_txtCarrera.text;
    agregarEstudiante.studentsPassword=_txtPassword.text;
    agregarEstudiante.studentsId=_txtCedula.text;
    
    
    [agregarEstudiante saveStudentInDatabase];
    
    
    _mensaje.text= agregarEstudiante.studentsStatus;
    
    [_mensaje setHidden:NO];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView *view in self.view.subviews) {
        [view resignFirstResponder];
    }
}
@end
