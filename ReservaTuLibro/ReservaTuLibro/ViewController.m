//
//  ViewController.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/12/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "ViewController.h"
#import "estudiante.h"
#import "reserva.h"


@interface ViewController (){
    estudiante *consultarEstudiante;
    NSString * cedula;
    NSString * password;
}

@end

@implementation ViewController

@synthesize cedula = _cedula;
@synthesize password = _password;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    reserva *reservas = [[reserva alloc]init];
    [reservas createDatabaseForReserva];
    
    consultarEstudiante =[[estudiante alloc]init];
    [consultarEstudiante createDatabaseForStudent];
    
	// Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)validar:(id)sender {
    
    cedula= _cedula.text;
 
    
    NSUserDefaults * defauls=[NSUserDefaults standardUserDefaults];
    [defauls setObject:cedula forKey:@"ced"];
    
    
  
    
    
    consultarEstudiante.studentsId=_cedula.text;
    consultarEstudiante.studentsPassword=_password.text;
    [consultarEstudiante searchStudensExist];
    
    BOOL exist = *(consultarEstudiante.studentsExist);
    
    
    if (!exist) {
        [_viewIngresar setHidden:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"El usuario no existe" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }else{
        [_viewIngresar setHidden:NO];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView *view in self.view.subviews) {
        [view resignFirstResponder];
    }
}
@end
