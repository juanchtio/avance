//
//  CancelarViewController.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 5/06/15.
//  Copyright (c) 2015 UDEM. All rights reserved.
//

#import "CancelarViewController.h"
#import "reserva.h"


@interface CancelarViewController (){
    reserva * consultarReserva;
    reserva * cancelarReserva;
}


@end

@implementation CancelarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Cancelar reserva";
    consultarReserva = [[reserva alloc]init];
    cancelarReserva =[[reserva alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)mostrar:(id)sender {
    consultarReserva.reservaId=_txtIdReserva.text;
    
    [consultarReserva searchReservaInDatabase];
    
    _IdReserva.text = consultarReserva.reservaId;
    _cedulaEstudiante.text = consultarReserva.reservaCedulaEstudiante;
    _libro.text = consultarReserva.reservaNombreLibro;
    _fechaReserva.text = consultarReserva.fechaReserva;
    _fechaDevolucion.text = consultarReserva.fechaDevolucion;
    
    [self.view endEditing:YES];
}

- (IBAction)cancelar:(id)sender {
    cancelarReserva.reservaId = _IdReserva.text;
    [cancelarReserva deleteEmployedInDataBase];
    
    _IdReserva.text=@"";
    
    _status.text = cancelarReserva.reservaStatus;
    
}
@end
