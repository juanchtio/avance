//
//  CancelarViewController.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 5/06/15.
//  Copyright (c) 2015 UDEM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelarViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtIdReserva;


@property (strong, nonatomic) IBOutlet UILabel *IdReserva;
@property (strong, nonatomic) IBOutlet UILabel *cedulaEstudiante;
@property (strong, nonatomic) IBOutlet UILabel *libro;
@property (strong, nonatomic) IBOutlet UILabel *fechaReserva;
@property (strong, nonatomic) IBOutlet UILabel *fechaDevolucion;

@property (strong, nonatomic) IBOutlet UILabel *status;


- (IBAction)mostrar:(id)sender;
- (IBAction)cancelar:(id)sender;
@end
