//
//  MostrarViewController.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/13/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "MostrarViewController.h"
#import "estudiante.h"

@interface MostrarViewController (){
    estudiante *consultarEstudiante;
}

@end

@implementation MostrarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    consultarEstudiante =[[estudiante alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)mostrar:(id)sender {
    consultarEstudiante.studentsId = _txtCedula.text;
    [consultarEstudiante searchStudentInDatabase];
    _lblNombre.text = consultarEstudiante.studentsName;
    _lblApellido.text = consultarEstudiante.studentsLastName;
    _lblCarrera.text = consultarEstudiante.studentsFaculty;
    _lblCedula.text = consultarEstudiante.studentsId;
    _lblPassword.text = consultarEstudiante.studentsPassword;
    
    [self.view endEditing:YES];
    
}
@end
