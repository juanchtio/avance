//
//  reserva.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/28/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "reserva.h"

@interface reserva(){
    NSInteger reserva_id;
    
}

@end

@implementation reserva

-(void) findDatabasePath{
    NSString * docsDir =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _dataBasePath = [[NSString alloc]initWithString:[docsDir stringByAppendingPathComponent:@"reservas.db"]];
    
}

//Creación de las tablas--------------------------------------------------------------------------------------------------------------------------------
//Crear la tabla RESERVA en la base de datos
-(void) createDatabaseForReserva{
    [self findDatabasePath];
    NSFileManager * fm = [[NSFileManager alloc]init];
    
    NSLog(@"%@", _dataBasePath);
    
    if ([fm fileExistsAtPath:_dataBasePath]==NO) {
        const char * dbpath=[_dataBasePath UTF8String];
        if(sqlite3_open(dbpath, &reservadb)==SQLITE_OK){
            NSLog(@"Base de datos creada con EXITO");
            char * errMsg;
            const char * sql_stmt = "CREATE TABLE IF NOT EXISTS RESERVAS (ID_RESERVA INTEGER PRIMARY KEY AUTOINCREMENT, RESERVA_STUDENT_CEDULA INTEGER, RESERVA_LIBRO_NAME TEXT, FECHA_RESERVA TEXT, FECHA_DEVOLUCION TEXT)";
            if (sqlite3_exec(reservadb, sql_stmt, NULL, NULL, &errMsg)==SQLITE_OK) {
                NSLog(@"Tabla creada con exito!");
            }else{
                NSLog(@"Error: %s", errMsg);
            }
            sqlite3_close(reservadb);
        }else{
            NSLog(@"La base de datos no se pudo crear");
        }
    }else{
        NSLog(@"El archivo ya existe!");
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
//CRUD para reservas------------------------------------------------------------------------------------------------------------------------------------------
-(void) saveReservaInDatabase{
    [self findDatabasePath];
    sqlite3_stmt *statement;
    const char * db= [_dataBasePath UTF8String];
    if (sqlite3_open(db, &reservadb)==SQLITE_OK) {
        NSString * insertSQL = [NSString stringWithFormat:@"INSERT INTO RESERVAS(RESERVA_STUDENT_CEDULA, RESERVA_LIBRO_NAME, FECHA_RESERVA, FECHA_DEVOLUCION)VALUES (\"%@\", \"%@\", \"%@\", \"%@\")", _reservaCedulaEstudiante, _reservaNombreLibro, _fechaReserva, _fechaDevolucion];
        const char * insert_sql =[insertSQL UTF8String];
        sqlite3_prepare_v2(reservadb, insert_sql, -1, &statement, NULL);
        if (sqlite3_step(statement)==SQLITE_DONE) {
             _reservaStatus = @"Registro Almacenado con exito";
            
        }else{
            _reservaStatus = @"Fallo al almacenar registro";
        }
        sqlite3_finalize(statement);
        sqlite3_close(reservadb);
    }else{
        _reservaStatus = @"Fallo al acceder a la base de datos";
    }
}

-(void) searchReservaInDatabase{
    [self findDatabasePath];
    sqlite3_stmt * query;
    const char * dbpath=[_dataBasePath UTF8String];
    if (sqlite3_open(dbpath, &reservadb)==SQLITE_OK) {
        NSString *select =[NSString stringWithFormat:@"SELECT * FROM RESERVAS WHERE ID_RESERVA = \"%@\"", _reservaId];
        const char * select_sql = [select UTF8String];
        if (sqlite3_prepare_v2(reservadb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            if (sqlite3_step(query)==SQLITE_ROW) {
                _reservaStatus = @"Registro Encontrado";
                _reservaId=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 0)];
                _reservaCedulaEstudiante=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 1)];
                _reservaNombreLibro=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 2)];
                _fechaReserva=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 3)];
                _fechaDevolucion=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 4)];
            }else{
                _reservaStatus = @"Registro no encontrado";
            }
            sqlite3_finalize(query);
        }else{
            _reservaStatus = @"Error al crear el query";
        }
        sqlite3_close(reservadb);
    }else{
        _reservaStatus =@"Fallo al acceder a la base de datos";
    }
}

-(void)deleteEmployedInDataBase{
    [self findDatabasePath];
    sqlite3_stmt * query;
    const char * db = [_dataBasePath UTF8String];
    if (sqlite3_open(db, &reservadb)==SQLITE_OK) {
        NSString *delete =[NSString stringWithFormat:@"DELETE FROM RESERVAS WHERE ID_RESERVA= \"%@\"", _reservaId];
        const char * delete_sql =[delete UTF8String];
        sqlite3_prepare_v2(reservadb, delete_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            _reservaStatus =@"Registro eliminado!";
        }else{
            _reservaStatus =@"Registro no existe";
            
        }
        sqlite3_finalize(query);
        sqlite3_close(reservadb);
    }else{
        _reservaStatus =@"Fallo al acceder a la base de datos";
    }
}
@end
