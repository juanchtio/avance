//
//  MostrarViewController.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/13/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "estudiante.h"

@interface MostrarViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtCedula;
- (IBAction)mostrar:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblNombre;
@property (weak, nonatomic) IBOutlet UILabel *lblApellido;
@property (weak, nonatomic) IBOutlet UILabel *lblCarrera;
@property (weak, nonatomic) IBOutlet UILabel *lblCedula;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;

@end
