//
//  estudiante.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/12/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "estudiante.h"

@interface estudiante() {
    
    NSInteger student_id;
}

@end

@implementation estudiante


-(void) findDatabasePath{
    NSString * docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _dataBasePath = [[NSString alloc]initWithString:[docsDir stringByAppendingPathComponent:@"students.db"]];
}

//Creación de las tablas----------------------------------------------------------------------------------------------------------------------------------------
//Crear la tabla STUDENTS en la base de datos
-(void) createDatabaseForStudent{
    [self findDatabasePath];
    NSFileManager * fm = [[NSFileManager alloc]init];
    NSLog(@"%@", _dataBasePath);
    
    if ([fm fileExistsAtPath:_dataBasePath]==NO) {
        
        const char * dbpath=[_dataBasePath UTF8String];
        
        if(sqlite3_open(dbpath, &alumnosdb)==SQLITE_OK){
            NSLog(@"Base de datos creada con EXITO");
            char * errMsg;
            const char * sql_stmt = "CREATE TABLE IF NOT EXISTS STUDENTS (STUDENT_NAME TEXT, STUDENT_LASTNAME TEXT, STUDENT_FACULTY TEXT, ID INTEGER PRIMARY KEY, STUDENT_PASSWORD TEXT)";
            if (sqlite3_exec(alumnosdb, sql_stmt, NULL, NULL, &errMsg)==SQLITE_OK) {
                NSLog(@"Tabla creada con exito!");
            }else{
                NSLog(@"Error: %s", errMsg);
            }
            sqlite3_close(alumnosdb);
        }else{
            NSLog(@"La base de datos no se pudo crear");
        }
    }else{
        NSLog(@"El archivo ya existe!");
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------

//CRUD para estudiantes------------------------------------------------------------------------------------------------------------------------------------------
-(void) saveStudentInDatabase{
    
    [self findDatabasePath];
    sqlite3_stmt *statement;
    
    const char * dbpath=[_dataBasePath UTF8String];
    
    if (sqlite3_open(dbpath, &alumnosdb)==SQLITE_OK) {
        NSString * insertSQL = [NSString stringWithFormat:@"INSERT INTO STUDENTS(STUDENT_NAME , STUDENT_LASTNAME , STUDENT_FACULTY , ID , STUDENT_PASSWORD)VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\")", _studentsName, _studentsLastName, _studentsFaculty, _studentsId, _studentsPassword];
        const char * insert_sql =[insertSQL UTF8String];
        sqlite3_prepare_v2(alumnosdb, insert_sql, -1, &statement, NULL);
        if (sqlite3_step(statement)==SQLITE_DONE) {
            _studentsStatus = @"Registro Almacenado con exito";
        }else{
            _studentsStatus =@"Fallo al almaenar registro";
        }
        sqlite3_finalize(statement);
        sqlite3_close(alumnosdb);
    }else{
        _studentsStatus =@"Fallo al acceder a la base de datos";
    }
}

-(void) searchStudentInDatabase{
    
    [self findDatabasePath];
    sqlite3_stmt * query;
    
    const char * dbpath=[_dataBasePath UTF8String];
    
    if (sqlite3_open(dbpath, &alumnosdb)==SQLITE_OK) {
        NSString *select =[NSString stringWithFormat:@"SELECT * FROM STUDENTS WHERE ID = \"%@\"", _studentsId];
        const char * select_sql = [select UTF8String];
        if (sqlite3_prepare_v2(alumnosdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            if (sqlite3_step(query)==SQLITE_ROW) {
                _studentsStatus=@"Registro Encontrado";
                _studentsName=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 0)];
                _studentsLastName=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 1)];
                _studentsFaculty=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 2)];
                _studentsId=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 3)];
                _studentsPassword=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 4)];
            }else{
                _studentsStatus = @"Registro no encontrado";
            }
            sqlite3_finalize(query);
        }else{
            _studentsStatus = @"Error al crear el query";
        }
        sqlite3_close(alumnosdb);
    }else{
        _studentsStatus =@"Fallo al acceder a la base de datos";
    }
}


-(void)searchStudensExist{
    [self findDatabasePath];
    sqlite3_stmt * query;
    
    const char * dbpath=[_dataBasePath UTF8String];
    
    if (sqlite3_open(dbpath, &alumnosdb)==SQLITE_OK) {
        NSString *select =[NSString stringWithFormat:@"SELECT * FROM STUDENTS WHERE ID = \"%@\" AND STUDENT_PASSWORD = \"%@\"", _studentsId , _studentsPassword] ;
        const char * select_sql = [select UTF8String];
        if (sqlite3_prepare_v2(alumnosdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            BOOL valor = NO;
            if (sqlite3_step(query)==SQLITE_ROW) {
                _studentsStatus=@"Registro Encontrado";
                _studentsName=[NSString stringWithFormat:@"%s",sqlite3_column_text(query, 0)];
                valor = YES;
            }
            _studentsExist = &valor;
            sqlite3_finalize(query);
        }else{
            _studentsStatus = @"Error al crear el query";
        }
        sqlite3_close(alumnosdb);
    }else{
        _studentsStatus =@"Fallo al acceder a la base de datos";
    }
}



//---------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------





@end
