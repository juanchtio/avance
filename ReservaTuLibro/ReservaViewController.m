//
//  ReservaViewController.m
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/28/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import "ReservaViewController.h"
#import "reserva.h"
#import "estudiante.h"

@interface ReservaViewController (){
    NSArray *books;
    NSInteger rowGlobal;
}

@end

@implementation ReservaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    agregarReserva = [[reserva alloc]init];
    estudi = [[estudiante alloc]init];
    
    //Cargar fechas
    int day,month,year,hour,minutes,seconds;
    
    
    
    NSDate *today = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    
    
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit| NSWeekCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:today];
    
    day = [dateComponents day];
    
    month = [dateComponents month];
    
    
    year = [dateComponents year];
    
    hour = [dateComponents hour];
    
    minutes = [dateComponents minute];
    
    seconds = [dateComponents second];
    
    // _fecha.text=[NSString stringWithFormat:@"%d",day];
    _fecha.text=[NSString stringWithFormat:@"%d/%d/%d", day, month, year];
    
    _fechaFinal.text=[NSString stringWithFormat:@"%d/%d/%d", day+10, month, year];
    
    
    
    //Cargar picker con los libros.
    NSString *path = [[NSBundle mainBundle]pathForResource:@"books" ofType:@"plist"];
    books = [NSArray arrayWithContentsOfFile:path];
    
    _librosPicker.delegate = self;
    _librosPicker.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

# pragma mark - UIPickerViewDataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return books.count;
}

# pragma mark - UIPickerViewDelegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Haz seleccionado " message:books[row] delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    rowGlobal=row;
    [alert show];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [books objectAtIndex:row];
}
- (IBAction)reserva:(id)sender {
    
    NSUserDefaults * defauls=[NSUserDefaults standardUserDefaults];
    
    agregarReserva.reservaId=agregarReserva.reservaId;
    agregarReserva.reservaCedulaEstudiante=(NSString*)[defauls objectForKey:@"ced"];
    agregarReserva.reservaNombreLibro=books[rowGlobal];
    agregarReserva.fechaReserva=_fecha.text;
    agregarReserva.fechaDevolucion=_fechaFinal.text;
    
    
    [agregarReserva saveReservaInDatabase];
    _mensajeS.text = agregarReserva.reservaStatus;
    [_mensajeS setHidden:NO];

    
}
@end
