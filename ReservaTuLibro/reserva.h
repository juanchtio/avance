//
//  reserva.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/28/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface reserva : NSObject{
    sqlite3 * reservadb;
}

@property NSString * dataBasePath;
@property NSString * reservaId;
@property NSString * reservaCedulaEstudiante;
@property NSString * reservaNombreLibro;
@property NSString * fechaReserva;
@property NSString * fechaDevolucion;
@property NSString * reservaStatus;
@property BOOL * reservaExist;


-(void) findDatabasePath;
-(void) createDatabaseForReserva;
-(void) saveReservaInDatabase;
-(void) searchReservaInDatabase;
-(void) searchReservaExist;
-(void)deleteEmployedInDataBase;

@end
