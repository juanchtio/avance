//
//  VerRerservasViewController.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/28/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "reserva.h"

@interface VerRerservasViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtIdReserva;

- (IBAction)mostrar:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *idReserva;
@property (weak, nonatomic) IBOutlet UILabel *cedulaEstudiante;
@property (weak, nonatomic) IBOutlet UILabel *libro;
@property (weak, nonatomic) IBOutlet UILabel *fechaReserva;
@property (weak, nonatomic) IBOutlet UILabel *fechaDevolucion;

@end
