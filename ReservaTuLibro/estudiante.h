//
//  estudiante.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/12/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface estudiante : NSObject{
    sqlite3 * alumnosdb;
}
@property NSString * dataBasePath;

@property NSString * studentsName;

@property NSString * studentsLastName;

@property NSString * studentsFaculty;

@property NSString * studentsId;

@property NSString * studentsPassword;

@property NSString * studentsStatus;

@property BOOL * studentsExist;


-(void) findDatabasePath;
-(void) createDatabaseForStudent;
-(void) saveStudentInDatabase;
-(void) searchStudentInDatabase;
-(void) searchStudensExist;

@end

