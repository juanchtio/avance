//
//  ReservaViewController.h
//  ReservaTuLibro
//
//  Created by centro docente de computos on 11/28/14.
//  Copyright (c) 2014 UDEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "reserva.h"
#import "estudiante.h"
#import "ViewController.h"

@interface ReservaViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>{
    reserva * agregarReserva;
    estudiante * estudi;
}

@property (weak, nonatomic) IBOutlet UIPickerView *librosPicker;
- (IBAction)reserva:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *fecha;
@property (weak, nonatomic) IBOutlet UILabel *fechaFinal;
@property (strong, nonatomic) IBOutlet UILabel *mensajeS;

@end
